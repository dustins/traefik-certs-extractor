FROM gitea.swigg.net/dustins/alpine:latest

ARG AUTHORS
ARG REPO_URL
ARG LICENSE

LABEL org.opencontainers.image.authors="${AUTHORS}"
LABEL org.opencontainers.image.url="${REPO_URL}"
LABEL org.opencontainers.image.licenses="${LICENSE}"

RUN apk add jq
RUN mkdir -p /workdir/output

WORKDIR /workdir

VOLUME /workdir/output

COPY traefik-cert-extractor.sh /etc/periodic/15min/
