#!/command/with-contenv sh

SOURCE=${SOURCE:-acme.json}
PRIVATE_KEY=${PRIVTAE_KEY:-privkey.pem}
FULL_CHAIN=${FULL_CHAIN:-fullchain.pem}
CERT_NAME=${CERT_NAME:-cert.pem}
CA_CHAIN=${CA_CHAIN:-cachain.pem}

cd /workdir

if [ ! -f "$SOURCE" ]; then
  echo "File $SOURCE does not exist"
  exit 1
fi

if [ `jq ". | has(\"$CERT_RESOLVER\")" $SOURCE` == "false" ]; then
  echo "$CERT_RESOLVER not found in $SOURCE"
  exit 1
fi

if [ -z `jq ".${CERT_RESOLVER}.Certificates[] | select(.domain.main==\"${DOMAIN_MAIN}\") | .key" $SOURCE` ]; then
  echo "$DOMAIN_MAIN  not found in $SOURCE"
  exit 1
fi

mkdir -v -p ./scratch

echo "Extracting $DOMAIN_MAIN certificate from $CERT_RESOLVER in $SOURCE"

echo "Extracting $PRIVATE_KEY from $SOURCE"
jq -r ".${CERT_RESOLVER}.Certificates[] | select(.domain.main==\"${DOMAIN_MAIN}\") | .key" $SOURCE | base64 -d > ./scratch/$PRIVATE_KEY

echo "Extracting $FULL_CHAIN from $SOURCE"
jq -r ".${CERT_RESOLVER}.Certificates[] | select(.domain.main==\"${DOMAIN_MAIN}\") | .certificate" $SOURCE | base64 -d > ./scratch/$FULL_CHAIN

echo "Splitting $FULL_CHAIN into seperate certificates"
awk 'BEGIN {c=0;} /BEGIN CERT/{c++} { print > "cert." c ".pem"}' < ./scratch/$FULL_CHAIN

echo "Moving first certificate to $CERT_NAME"
mv cert.1.pem ./scratch/$CERT_NAME

echo "Concatenating remaining certificates to $CA_CHAIN"
cat cert.*.pem > ./scratch/$CA_CHAIN

if [ -n "$CHOWN" ]; then
  echo "Changing ownership of certificates to $CHOWN"
  chown -R $CHOWN /workdir/scratch
fi

mv -v ./scratch/* ./output/
rm -rfv ./scratch
